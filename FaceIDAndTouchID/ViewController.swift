//  ViewController.swift
//  FaceIDAndTouchID
//  Created by Viktoriia Skvarko on 22.02.2021.


import UIKit
import LocalAuthentication

class ViewController: UIViewController {
    
    @IBOutlet weak var labeTitle: UILabel!
    
    @IBAction func autorizationAction(_ sender: Any) {
        indetifyYourself()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labeTitle.text = "Идентифицируйте себя"
        
    }
    
    private func indetifyYourself() {
        let context = LAContext()   // контекст, который обеспечивает взаимодействие между приложением и биометрическими функциями
        var error: NSError?    // переменная типа NSError, которая опционально будет хранить ошибки
        
        // проверяем, есть ли у пользователя техническая возможность провести биометрическую аутентификацию:
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            
            let reason = labeTitle.text! 
            context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reason ) { success, error in  // запускаем процесс аутентификации
                
                if success {
                    DispatchQueue.main.async { [unowned self] in  // если авторизация прошла успешно, сообщаем об этом пользователю через DispatchQueue.main.async
                        labeTitle.text = "Успешная авторизация"
                    }
                }
            }
            
        } else {
            labeTitle.text = "Face/Touch ID не найден"
        }
    }
    
}

